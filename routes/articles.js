const express = require('express');
const router = express.Router();
const debug = require('debug')('router:articles');
const ArticleController = require("../controller/ArticleController");
const controller = new ArticleController();

router.get("/", function (req, res, next) {
    const articles = controller.getAll();
    res.send(articles);
});
router.get('/:id', (req, res, next) => {
    const id = req.params.id
    try{
    const article = controller.getOne(id);
    res.send(article);
    }catch (e){

    }
});
router.post("/", (req, res) => {
    const body = req.body;
    console.log(body);
    const article = {
        image: body.image,
        title: body.title,
        date: new Date().toISOString(),
        text: body.text
    }

    const stm = db.prepare("INSERT INTO article (image, title, date, text) VALUES (?,?,?,?)");
    stm.run(...Object.values(article))
    res.send(article);
});
router.patch("/:id", (req, res) => {
    const body = req.body;
    const id = req.params.id;
    if (id) {
        const article = db.prepare('SELECT * FROM article WHERE id = ?').get(id);
        if (article) {
            Object.assign(article, body);
            const stm = db.prepare(
                "UPDATE article SET image = ?, title = ?, date = ?, text = ? WHERE id = ?"
            );
            stm.run(article.image, article.title,article.date,article.text,parseInt(id));
        } else {
            res.sendStatus(404)
        }
        res.send(article);
    } else {
        res.sendStatus(404);
    }
});
router.delete("/:id", (req, res) => {
    const id = req.params.id;
    if (id) {
        db.prepare("DELETE FROM article WHERE id = ?").run(id)
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
})
module.exports = router;